﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Lab1;
using MathNet.Numerics.LinearAlgebra;
using System.IO;

namespace Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestDense()
        {
            Matrix<double> DenseA = Matrix<double>.Build.Dense(100, 130, (i, j) => 5.2 * (i - j));

            Matrix<double> DenseB = Matrix<double>.Build.Dense(130, 1, (i, j) => (i + 4) * 2);

            Matrix<double> ExpectedDense = Matrix<double>.Build.DenseOfArray(new double[,] {{-7877428, -7784816, -7692204, -7599592,
                                         -7506980, -7414368, -7321756, -7229144, -7136532, -7043920, -6951308, -6858696 ,
                                          -6766084, -6673472,  -6580860,  -6488248,  -6395636,  -6303024,  -6210412,  -6117800 ,
                                          -6025188,  -5932576,  -5839964,  -5747352,  -5654740,  -5562128,  -5469516,  -5376904 ,
                                          -5284292,  -5191680,  -5099068,  -5006456,  -4913844,  -4821232,  -4728620,  -4636008 ,
                                          -4543396,  -4450784,  -4358172,  -4265560,  -4172948,  -4080336,  -3987724,  -3895112 ,
                                          -3802500,  -3709888,  -3617276,  -3524664,  -3432052,  -3339440,  -3246828,  -3154216 ,
                                          -3061604,  -2968992,  -2876380,  -2783768,  -2691156,  -2598544,  -2505932,  -2413320 ,
                                          -2320708,  -2228096,  -2135484,  -2042872,  -1950260,  -1857648,  -1765036,  -1672424 ,
                                          -1579812,  -1487200,  -1394588,  -1301976,  -1209364,  -1116752,  -1024140,  -931528 ,
                                          -838916,  -746304,  -653692,  -561080,  -468468,  -375856,  -283244,  -190632 ,
                                          -98020,  -5408 , 87204 , 179816 , 272428 , 365040 , 457652 , 550264 ,
                                          642876 , 735488 , 828100 , 920712 , 1013324 , 1105936,1198548 , 1291160 }});

            Calculation с = new Calculation();
            Matrix<double> res = с.MultiplyMatrices(DenseA,DenseB);

            Assert.AreEqual(true, с.CompareMatrices(res, ExpectedDense));
        }

        [TestMethod]
        public void TestSparse()
        {
            Matrix<double> SparseA = Matrix<double>.Build.Sparse(100, 130, (i, j) => i == j ? 10 : 0);

            Matrix<double> SparseB = Matrix<double>.Build.Sparse(130, 1, (i, j) => i == j ? 10 : 0);

            Matrix<double> ExpectedSparse = Matrix<double>.Build.Sparse(1, 100, (i, j) => (i == 0) && (j == 0) ? 100 : 0);

            Calculation с = new Calculation();
            Matrix<double> res = с.MultiplyMatrices(SparseA, SparseB);

            Assert.AreEqual(true, с.CompareMatrices(res, ExpectedSparse));
        }

        [TestMethod]
        public void TestDense2()
        {
            Matrix<double> DenseA = Matrix<double>.Build.Dense(5, 5, (i, j) => 5.2 * (i - j));

            Matrix<double> DenseB = Matrix<double>.Build.Dense(5, 5, (i, j) => (i - j) * 5);

            Matrix<double> ExpectedDense = Matrix<double>.Build.DenseOfArray(new double[,] {{-780,   -520,    -260,    0,    260},
                                                                                            {-520,   -390,    -260,    -130,    0},
                                                                                            {-260,   -260,    -260,    -260,    -260},
                                                                                            {0,    -130,    -260,    -390,   -520},
                                                                                            {260,   0,   -260,  -520,   -780}});

            Calculation с = new Calculation();
            Matrix<double> res = с.MultiplyMatrices(DenseA, DenseB);

            Assert.AreEqual(true, с.CompareMatrices(res, ExpectedDense));
        }
    }
}
