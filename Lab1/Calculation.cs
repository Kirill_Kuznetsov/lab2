﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;


namespace Lab1
{
    public class Calculation: INumericalIntegration
    {
        public Matrix<double> MultiplyMatrices(Matrix<double> A, Matrix<double> B)
            => (A * B).Transpose();

        public bool CompareMatrices(Matrix<double> A, Matrix<double> B)
        {
            for (int i = 0; i < A.RowCount; i++)
            {
                for (int j = 0; j < A.ColumnCount; j++)
                {
                    if (Math.Round(A[i, j]) != Math.Round(B[i, j]))
                    {
                        return false;
                    }
                }
            }

          return true;
        }
            
    }
}
