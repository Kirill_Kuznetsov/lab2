﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public interface INumericalIntegration
    {
        Matrix<double> MultiplyMatrices(Matrix<double> A, Matrix<double> B);

        bool CompareMatrices(Matrix<double> A, Matrix<double> B);
    }
}
