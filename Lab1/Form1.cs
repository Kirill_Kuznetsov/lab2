﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Symbolics;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private Matrix<double> Matrix() 
        {
            Calculation c = new Calculation();
            Matrix<double> A = Matrix<double>.Build.Dense(Convert.ToInt32(m1.Text), Convert.ToInt32(n1.Text), (i, j) => 5.2 * (i - j));

            StreamWriter writer = new StreamWriter("C:\\Users\\kiril\\Downloads\\Lab2\\Lab1\\Lab1\\bin\\Debug\\A.txt", true);

            for (int i = 0; i < Convert.ToInt32(m1.Text); i++)
            {
                for (int j = 0; j < Convert.ToInt32(n1.Text); j++)
                    writer.Write(A[i,j] + " ");
            }
            writer.Close();

            Matrix<double> B = Matrix<double>.Build.Dense(Convert.ToInt32(m2.Text), Convert.ToInt32(n2.Text), (i, j) => (i + 4) * 2);

            writer = new StreamWriter("C:\\Users\\kiril\\Downloads\\Lab2\\Lab1\\Lab1\\bin\\Debug\\B.txt", true);

            for (int i = 0; i < Convert.ToInt32(m2.Text); i++)
            {
                for (int j = 0; j < Convert.ToInt32(n2.Text); j++)
                    writer.Write(B[i,j] + " ");
            }
            writer.Close();

            return c.MultiplyMatrices(A, B);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Matrix<double> res = Matrix();

            StreamWriter writer = new StreamWriter("C:\\Users\\kiril\\Downloads\\Lab2\\Lab1\\Lab1\\bin\\Debug\\C.txt", true);

            for (int i = 0; i < Convert.ToInt32(m1.Text); i++)
            {
                for (int j = 0; j < Convert.ToInt32(n2.Text); j++)
                    writer.Write("[" + i + "," + j + "] " + Math.Round(res[j, i]) + "");
                writer.Write("\n");
            }
            writer.Close();

            richTextBox1.Text = Convert.ToString(res);
        }
    }
}
